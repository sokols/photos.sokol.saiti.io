upload_dir=/........./uploads/*.jpg
destination_dir=/........./photos/
counter=0

for file in $upload_dir; do

new_file_name=$(date +'%Y%m%d%H%M%S')

counter=$(expr $counter + 1)

mv $file $destination_dir$new_file_name$counter.jpg

done