package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
)

type configuration struct {
	ServerPort string
}

type photoList struct {
	FileNames       []string
	hasNextPage     bool
	hasPreviousPage bool
	CurrentPage     int
}

func main() {

	serverConfiguration := readConfiguration()

	incrementFunction := template.FuncMap{
		"increment": func(i int) int {
			return i + 1
		},
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var page int
		var e error

		page, e = strconv.Atoi(r.URL.Query().Get("p"))

		if e != nil {
			page = 1
		}

		t, parseError := template.New("index.html").Funcs(incrementFunction).ParseFiles("templates/index.html")

		if parseError != nil {
			log.Fatalln(parseError)
		}

		filePage := paginateSlice(listImageFiles(), page, 3)
		log.Println(filePage)
		err := t.Execute(w, filePage)

		if err != nil {
			log.Fatalln(err)
		}
	})
	http.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {

		http.ServeFile(w, r, "styles/style.css")

	})

	http.HandleFunc("/images/", func(w http.ResponseWriter, r *http.Request) {

		urlPath := r.URL.Path
		http.ServeFile(w, r, strings.Replace(urlPath, "/images", "photos", -1))
	})

	log.Println("Server Started!")
	log.Fatal(http.ListenAndServe(serverConfiguration.ServerPort, nil))
}

func listImageFiles() photoList {
	files, _ := ioutil.ReadDir("photos")
	var list photoList
	for _, f := range files {
		list.FileNames = append(list.FileNames, f.Name())

	}

	sort.Slice(list.FileNames, func(i, j int) bool {
		return list.FileNames[i] > list.FileNames[j]
	})

	return list
}

func paginateSlice(array photoList, page int, noOfEntries int) photoList {
	var filePage photoList
	if page > len(array.FileNames) {
		page = len(array.FileNames)
	}

	end := page + noOfEntries

	if end > len(array.FileNames) {
		end = len(array.FileNames)
		filePage.hasNextPage = false
	}
	filePage.CurrentPage = page
	filePage.FileNames = array.FileNames[page:end]
	return filePage
}

func readConfiguration() configuration {
	file, e := os.Open("c.json")
	if e != nil {
		fmt.Println("Cannot read configuration file.", e)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	conf := configuration{}
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("Cannot read configuration.", err)
	}

	return conf
}
